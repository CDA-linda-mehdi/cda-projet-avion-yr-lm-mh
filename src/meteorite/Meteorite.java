package meteorite;

public class Meteorite {
	
	private int pv = 100;

	private int posX;
	private int posY;
	
	public Meteorite(int px, int py) {
		posX = px;
		posY = py;
	}
	
	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

}
