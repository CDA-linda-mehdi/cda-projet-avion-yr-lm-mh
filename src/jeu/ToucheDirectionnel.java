package jeu;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ToucheDirectionnel implements KeyListener {
	// Variable de directions
		private int deplacementX;
		private int deplacementY;
		//Touches actuellement utiliser (Direction des touches presser)
		private boolean droite = false;
		private boolean gauche = false;
		private boolean haut = false;
		private boolean bas = false;
		
		
		
		public void direction() {
			
			if(droite)				//Direction X
				deplacementX = 3;
			else if(gauche)
				deplacementX = -3;
			else
				deplacementX = 0;
			
			if(haut)				//Direction Y
				deplacementY = -3;
			else if(bas)
				deplacementY = 3;
			else
				deplacementY = 0;
			
			
			if(droite && gauche) {  //Si on presse deux touche oppose le deplacement  est nul
		    	deplacementX = 0;
		    }
			
		    if(haut && bas) {
		    	deplacementX = 0;
		    }
		    
		    if(droite && haut && !bas && !gauche) {  //Deplacement en diagonales 
		    	deplacementX = 2;
		    	deplacementY = -2;
		    }
		    if(!droite && haut && !bas && gauche) {
		    	deplacementX = -2;
		    	deplacementY = -2;
		    }
		    if(droite && !haut && bas && !gauche) {
		    	deplacementX = 2;
		    	deplacementY = 2;
		    }
		    if(!droite && !haut && bas && gauche) {
		    	deplacementX = -2;
		    	deplacementY = 2;
		    }
		    
		 
		    
		    
		}

	    public void keyPressed (KeyEvent e){
	    	
		    if(e.getKeyCode()==KeyEvent.VK_UP) {
		    	haut = true;
		    	System.out.println("haut");
		    	}
		    if(e.getKeyCode()==KeyEvent.VK_DOWN) {
		    	bas = true;
		    	System.out.println("bas");
		    	}
		    if(e.getKeyCode()==KeyEvent.VK_RIGHT){       
		    	droite = true;
		    	System.out.println("droite");
		    	}
		    if(e.getKeyCode()==KeyEvent.VK_LEFT) {
		    	gauche = true;
		    	System.out.println("gauche");
		    	}
		   
		    
		    direction(); //Nouvelle direction en fonction des nouvelles touches
		    

	    }
	    
	    public void keyReleased (KeyEvent e){
	    	
	    	if(e.getKeyCode()==KeyEvent.VK_UP) {
	    		haut = false;
	    		System.out.println("fin haut");
	    	}
		    
	    	if(e.getKeyCode()==KeyEvent.VK_DOWN) {
		    	bas = false;
		    	System.out.println("fin bas");
	    	}
		    	
		    if(e.getKeyCode()==KeyEvent.VK_RIGHT) {            
		    	droite = false;
		    	System.out.println("fin droite");
		    }
		    	
		    if(e.getKeyCode()==KeyEvent.VK_LEFT) {
		    	gauche = false;
		    	System.out.println("fin gauche");
		    }
		    
		    
	    	direction(); //Nouvelle direction en fonction des nouvelles touches
	    }
	    
	    public void keyTyped (KeyEvent e){}
	    
	    public int getDeplacementX() {
	    	return deplacementX;
	    }
	    public int getDeplacementY() {
	    	return deplacementY;
	    }
	    }