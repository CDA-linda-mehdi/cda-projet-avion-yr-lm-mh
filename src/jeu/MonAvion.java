package jeu;

public class MonAvion {
	
	int PositionX;
	int PositionY;
	int PV;
	int taille;
	int vie = 5;
	
	public MonAvion(){
		this.PositionX = 0;
		this.PositionY = 0;
		this.PV = 100;
		this.taille = 10;
	}
	
	public MonAvion(int x, int y, int pv){
		this.PositionX = x;
		this.PositionY = y;
		this.PV = pv;
		this.taille = 10;
	}

	
	
	public int getPositionX() {
		return PositionX;
	}

	public void setPositionX(int positionX) {
		PositionX = positionX;
	}

	public int getPositionY() {
		return PositionY;
	}

	public void setPositionY(int positionY) {
		PositionY = positionY;
	}

	public double getPV() {
		return PV;
	}

	public void setPV(int pV) {
		PV = pV;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}
	

}
