package jeu;

import java.io.IOException;

import javax.swing.JFrame;

public class Fenetre extends JFrame {

	private ToucheDirectionnel ke = new ToucheDirectionnel();
	private MonPanel pan = new MonPanel();
	Image image = new Image();
	private static final long serialVersionUID = 1L;

	public Fenetre(String titre) throws IOException {

		this.setTitle(titre);
		this.setSize(500, 700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.addKeyListener(ke);
		this.setContentPane(pan);
		this.add(image);

		this.setVisible(true);
		go();
	}

	private void go() {

		int nbtick = 0;
		while(true) {
			nbtick++;
			//Verification que l'avion reste dans le cadre de la fenetre pour bouger
			if(nbtick%5 == 0) {
				if((pan.getPosX()+ke.getDeplacementX()*pan.getCoefVitesseX() < (this.getWidth()-50))&& (pan.getPosX()+ke.getDeplacementX()*pan.getCoefVitesseX() > 0)) {
					pan.deplacementAvionX(ke.getDeplacementX());
				}
				if((pan.getPosY()+ke.getDeplacementY()*pan.getCoefVitesseY() < (this.getHeight()-75))&& (pan.getPosY()+ke.getDeplacementY()*pan.getCoefVitesseY() > 0)) {
					pan.deplacementAvionY(ke.getDeplacementY());
				}
			}
		
		pan.repaint();

		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	}
}
