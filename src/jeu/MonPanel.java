package jeu;

import java.awt.Graphics;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import traitement.ImageTaille;

public class MonPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int PosX = 225;
	private int PosY = 625;
	private int coefVitesseX = 1;
	private int coefVitesseY = 1;

	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		BufferedImage img2 = new ImageTaille().scaleImage(50, 50, "ressources/avion.jpg");
		g.drawImage(img2, this.PosX, this.PosY, this);
		
		
	}

	public void deplacementAvionX(int x) {
		this.PosX += coefVitesseX * x;
	}

	public void deplacementAvionY(int y) {
		this.PosY += coefVitesseX * y;
	}

	public int getCoefVitesseX() {
		return coefVitesseX;
	}

	public void setCoefVitesseX(int coefVitesseX) {
		this.coefVitesseX = coefVitesseX;
	}

	public int getCoefVitesseY() {
		return coefVitesseY;
	}

	public void setCoefVitesseY(int coefVitesseY) {
		this.coefVitesseY = coefVitesseY;
	}

	public int getPosX() {
		return PosX;
	}

	public void setPosX(int posX) {
		PosX = posX;
	}

	public int getPosY() {
		return PosY;
	}

	public void setPosY(int posY) {
		PosY = posY;
	}

}